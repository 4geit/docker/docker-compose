FROM docker
MAINTAINER caner@candan.fr

RUN \
    apk -U add py2-pip && \
    pip install docker-compose

CMD \
    docker-compose down && \
    docker-compose build && \
    docker-compose up -d
